const Book = require('../models/book.model');

const getAllBooks = (req, res) => {
  Book.find({}, (err, books) => {
    if (err) {
      res.status(500).json({ message: err });
    } else {
      res.status(200).json(books);
    }
  });
};

const getBook = (req, res) => {
  Book.findOne({ id: req.params.id }, (err, book) => {
    if (err) {
      res.status(500).json({ message: err });
    } else if (book === null) {
      res.status(404).json({ message: "Book doesn't exist" });
    } else {
      res.status(200).json(book);
    }
  });
};

const createBook = (req, res) => {
  Book.estimatedDocumentCount((errEstimate, count) => {
    if (errEstimate) {
      res.status(500).json({ message: errEstimate });
    } else if (req.body.title === undefined || req.body.author === undefined
        || req.body.isbn === undefined || req.body.publishedOn === undefined
        || req.body.numberOfPages === undefined) {
      res.status(400).json({ message: 'Bad request' });
    } else {
      const bookData = {
        id: count + 1,
        title: req.body.title,
        author: req.body.author,
        isbn: req.body.isbn,
        publishedOn: req.body.publishedOn,
        numberOfPages: req.body.numberOfPages,
      };

      Book.create(bookData, (errCreate, book) => {
        if (errCreate) {
          res.status(500).json({ message: errCreate });
        } else {
          res.status(201).json({ id: book.id });
        }
      });
    }
  });
};

const updateBook = (req, res) => {
  if (req.body.title === undefined || req.body.author === undefined
    || req.body.isbn === undefined || req.body.publishedOn === undefined
    || req.body.numberOfPages === undefined) {
    res.status(400).json({ message: 'Bad request' });
  } else {
    Book.exists({ id: req.params.id }, (errExists, exist) => {
      if (errExists) {
        res.status(500).json({ message: errExists });
      } else if (!exist) {
        res.status(404).json({ message: "Book doesn't exist" });
      } else {
        const book = {
          id: req.params.id,
          title: req.body.title,
          author: req.body.author,
          isbn: req.body.isbn,
          publishedOn: req.body.publishedOn,
          numberOfPages: req.body.numberOfPages,
        };

        Book.replaceOne({ id: req.params.id }, book, (errReplace) => {
          if (errReplace) {
            res.status(500).json({ message: errReplace });
          } else {
            res.status(200).send();
          }
        });
      }
    });
  }
};

const deleteBook = (req, res) => {
  Book.exists({ id: req.params.id }, (errExists, exist) => {
    if (errExists) {
      res.status(500).json({ message: errExists });
    } else if (!exist) {
      res.status(404).json({ message: "Book doesn't exist" });
    } else {
      Book.deleteOne({ id: req.params.id }, (errDelete) => {
        if (errDelete) {
          res.status(500).json({ message: errDelete });
        } else {
          res.status(204).send();
        }
      });
    }
  });
};

module.exports = {
  getAllBooks,
  getBook,
  createBook,
  updateBook,
  deleteBook,
};
