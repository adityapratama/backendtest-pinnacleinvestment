const mongoose = require('mongoose');

const { Schema } = mongoose;

const BookSchema = new Schema({
  id: {
    type: Number,
    unique: true,
    required: true,
  },
  title: {
    type: String,
    unique: false,
    required: true,
  },
  author: {
    type: String,
    unique: false,
    required: true,
  },
  isbn: {
    type: String,
    unique: false,
    required: true,
  },
  publishedOn: {
    type: Number,
    unique: false,
    required: true,
  },
  numberOfPages: {
    type: Number,
    unique: false,
    required: true,
  },
});

const BookModel = mongoose.model('Book', BookSchema);

module.exports = BookModel;
