const { Router } = require('express');
const controller = require('../controllers/book.controller');

const router = Router();

router.get('/books', controller.getAllBooks);
router.get('/books/:id(\\d+)', controller.getBook);
router.post('/books', controller.createBook);
router.put('/books/:id(\\d+)', controller.updateBook);
router.delete('/books/:id(\\d+)', controller.deleteBook);

module.exports = router;
