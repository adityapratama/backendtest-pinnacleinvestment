const { expect } = require('chai');
const sinon = require('sinon');
const mongoose = require('mongoose');
const bookController = require('../controllers/book.controller');
const reqMocker = require('./mocks/request.mock');
const resMocker = require('./mocks/response.mock');
const booksArrayFixture = require('./fixtures/listBooks.fixture.json');
const newOrUpdateBooksFixture = require('./fixtures/newOrUpdateBook.fixture.json');

let req;
let res;

beforeEach(() => {
  req = reqMocker;
  res = resMocker;
});

afterEach(() => {
  sinon.restore();
  req.params = {};
  req.body = {};
  res.body = null;
  res.statuCode = null;
});

describe('GET /books', () => {
  it('should return all books', async () => {
    sinon.stub(mongoose.Model, 'find').yields(null, booksArrayFixture);
    bookController.getAllBooks(req, res);

    expect(res.statusCode).to.equal(200);
    expect(res.body.length).to.equal(2);
  });
  it('should return empty array', async () => {
    sinon.stub(mongoose.Model, 'find').yields(null, []);
    bookController.getAllBooks(req, res);

    expect(res.statusCode).to.equal(200);
    expect(res.body.length).to.equal(0);
    expect(res.body).to.eql([]);
  });
  it('should return error', async () => {
    sinon.stub(mongoose.Model, 'find').yields({ message: 'MongoError' }, null);
    bookController.getAllBooks(req, res);

    expect(res.statusCode).to.equal(500);
    expect(res.body).to.have.property('message');
  });
});

describe('GET /books/:id', () => {
  it('should return book with specific id', async () => {
    req.params.id = 1;
    sinon.stub(mongoose.Model, 'findOne').yields(null, booksArrayFixture[0]);
    bookController.getBook(req, res);

    expect(res.statusCode).to.equal(200);
    expect(res.body.id).to.equal(1);
  });
  it('should return 404 not found', async () => {
    req.params.id = 3;
    sinon.stub(mongoose.Model, 'findOne').yields(null, null);
    bookController.getBook(req, res);

    expect(res.statusCode).to.equal(404);
    expect(res.body).to.have.property('message');
  });
  it('should return error', async () => {
    sinon.stub(mongoose.Model, 'findOne').yields({ message: 'MongoError' }, null);
    bookController.getBook(req, res);

    expect(res.statusCode).to.equal(500);
    expect(res.body).to.have.property('message');
  });
});

describe('POST /books', () => {
  const countDocuments = 2;
  it('should create new book and return the generated id', async () => {
    sinon.stub(mongoose.Model, 'estimatedDocumentCount').yields(null, countDocuments);
    sinon.stub(mongoose.Model, 'create').yields(null, newOrUpdateBooksFixture);
    req.body = newOrUpdateBooksFixture;
    bookController.createBook(req, res);
    expect(res.statusCode).to.equal(201);
    expect(res.body).to.have.property('id');
    expect(res.body.id).to.equal(newOrUpdateBooksFixture.id);
  });
  it('should return 400 bad request', async () => {
    sinon.stub(mongoose.Model, 'estimatedDocumentCount').yields(null, countDocuments);
    sinon.stub(mongoose.Model, 'create').yields(null, []);
    bookController.createBook(req, res);
    expect(res.statusCode).to.equal(400);
    expect(res.body).to.have.property('message');
  });
  it('should return error on model estimatedDocumentCount method', async () => {
    sinon.stub(mongoose.Model, 'estimatedDocumentCount').yields({ message: 'MongoError' }, null);
    bookController.createBook(req, res);
    expect(res.statusCode).to.equal(500);
    expect(res.body).to.have.property('message');
  });
  it('should return error on model create method', async () => {
    sinon.stub(mongoose.Model, 'estimatedDocumentCount').yields(null, countDocuments);
    sinon.stub(mongoose.Model, 'create').yields({ message: 'MongoError' }, null);
    req.body = {
      title: newOrUpdateBooksFixture.title,
      author: newOrUpdateBooksFixture.author,
      isbn: newOrUpdateBooksFixture.isbn,
      publishedOn: newOrUpdateBooksFixture.publishedOn,
      numberOfPages: newOrUpdateBooksFixture.numberOfPages,
    };
    bookController.createBook(req, res);
    expect(res.statusCode).to.equal(500);
    expect(res.body).to.have.property('message');
  });
});

describe('PUT /books/:id', () => {
  it('should update a book', async () => {
    req.params.id = 3;
    sinon.stub(mongoose.Model, 'exists').yields(null, true);
    sinon.stub(mongoose.Model, 'replaceOne').yields(null);
    req.body = {
      title: newOrUpdateBooksFixture.title,
      author: newOrUpdateBooksFixture.author,
      isbn: newOrUpdateBooksFixture.isbn,
      publishedOn: newOrUpdateBooksFixture.publishedOn,
      numberOfPages: newOrUpdateBooksFixture.numberOfPages,
    };
    bookController.updateBook(req, res);
    expect(res.statusCode).to.equal(200);
    expect(res.body).to.equal(undefined);
  });
  it('should return 404 not found', async () => {
    req.params.id = 3;
    sinon.stub(mongoose.Model, 'exists').yields(null, false);
    req.body = {
      title: newOrUpdateBooksFixture.title,
      author: newOrUpdateBooksFixture.author,
      isbn: newOrUpdateBooksFixture.isbn,
      publishedOn: newOrUpdateBooksFixture.publishedOn,
      numberOfPages: newOrUpdateBooksFixture.numberOfPages,
    };
    bookController.updateBook(req, res);
    expect(res.statusCode).to.equal(404);
    expect(res.body).to.have.property('message');
  });
  it('should return 400 bad request', async () => {
    req.body = [];
    bookController.updateBook(req, res);
    expect(res.statusCode).to.equal(400);
    expect(res.body).to.have.property('message');
  });
  it('should return error on model exists method', async () => {
    req.params.id = 3;
    sinon.stub(mongoose.Model, 'exists').yields({ message: 'MongoError' }, null);
    req.body = {
      title: newOrUpdateBooksFixture.title,
      author: newOrUpdateBooksFixture.author,
      isbn: newOrUpdateBooksFixture.isbn,
      publishedOn: newOrUpdateBooksFixture.publishedOn,
      numberOfPages: newOrUpdateBooksFixture.numberOfPages,
    };
    bookController.updateBook(req, res);
    expect(res.statusCode).to.equal(500);
    expect(res.body).to.have.property('message');
  });
  it('should return error on model replaceOne method', async () => {
    req.params.id = 3;
    sinon.stub(mongoose.Model, 'exists').yields(null, true);
    sinon.stub(mongoose.Model, 'replaceOne').yields({ message: 'MongoError' });
    req.body = {
      title: newOrUpdateBooksFixture.title,
      author: newOrUpdateBooksFixture.author,
      isbn: newOrUpdateBooksFixture.isbn,
      publishedOn: newOrUpdateBooksFixture.publishedOn,
      numberOfPages: newOrUpdateBooksFixture.numberOfPages,
    };
    bookController.updateBook(req, res);
    expect(res.statusCode).to.equal(500);
    expect(res.body).to.have.property('message');
  });
});

describe('DELETE /books/:id', () => {
  it('should delete a book', async () => {
    req.params.id = 3;
    sinon.stub(mongoose.Model, 'exists').yields(null, true);
    sinon.stub(mongoose.Model, 'deleteOne').yields(null);
    bookController.deleteBook(req, res);
    expect(res.statusCode).to.equal(204);
  });
  it('should return 404 not found', async () => {
    req.params.id = 3;
    sinon.stub(mongoose.Model, 'exists').yields(null, false);
    sinon.stub(mongoose.Model, 'deleteOne').yields(null);
    bookController.deleteBook(req, res);
    expect(res.statusCode).to.equal(404);
  });
  it('should return error on model exists method', async () => {
    req.params.id = 3;
    sinon.stub(mongoose.Model, 'exists').yields({ message: 'MongoError' }, true);
    bookController.deleteBook(req, res);
    expect(res.statusCode).to.equal(500);
    expect(res.body).to.have.property('message');
  });
  it('should return error on model deleteOne method', async () => {
    req.params.id = 3;
    sinon.stub(mongoose.Model, 'exists').yields(null, true);
    sinon.stub(mongoose.Model, 'deleteOne').yields({ message: 'MongoError' });
    bookController.deleteBook(req, res);
    expect(res.statusCode).to.equal(500);
    expect(res.body).to.have.property('message');
  });
});
