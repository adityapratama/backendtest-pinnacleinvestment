const resMock = {
  body: null,
  statusCode: null,
  status(code) {
    this.statusCode = code;
    return this;
  },
  json(message) {
    this.body = message;
    return this;
  },
  send(message) {
    this.body = message;
    return this;
  },
};

module.exports = resMock;
