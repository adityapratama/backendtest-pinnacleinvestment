const mongoose = require('mongoose');

const connect = () => {
  mongoose.connect('mongodb://localhost/books', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  });
};

const disconnect = () => {
  mongoose.connection.close();
};

module.exports = {
  connect,
  disconnect,
};
