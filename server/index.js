const express = require('express');
const bodyParser = require('body-parser');

const router = require('../routes');
const db = require('../config/database');

db.connect();

const server = express();

server.use(express.json());

server.use(bodyParser.urlencoded({ extended: false }));

server.use(router);

module.exports = server;
